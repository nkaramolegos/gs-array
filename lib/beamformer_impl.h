/* -*- c++ -*- */
/*
 * Copyright 2019 gr-gsarray author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_GSARRAY_BEAMFORMER_IMPL_H
#define INCLUDED_GSARRAY_BEAMFORMER_IMPL_H

#include <vector>

#include <armadillo>
#define ARMA_DONT_PRINT_ERRORS
#include <gsarray/beamformer.h>
#include <gsarray/algorithm.h>
#include <gsarray/lcmv.h>
#include <gsarray/phaseshift.h>

namespace gr
{
namespace gsarray
{

class beamformer_impl : public beamformer
{
private:
  size_t d_num_streams;
  std::vector<std::vector<arma::cx_fvec>> d_steer_vecs
    { 361, std::vector<arma::cx_fvec> (91) };
  uint8_t d_array_t;
  double d_azimuth;
  double d_elevation;
  double d_spacing;
  double d_lambda;
  std::vector<std::vector<double>> d_positions;
  pmt::pmt_t d_coord_in;
  uint8_t d_method;

  algorithm *d_beamformer;

  void
  update_coordinates (pmt::pmt_t msg);

public:
  beamformer_impl (size_t num_streams, uint8_t array_t, double spacing,
                   double lambda, uint8_t method);
  ~beamformer_impl ();

  // Where all the action really happens
  int
  work (int noutput_items, gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items);
};

} // namespace gsarray
} // namespace gr

#endif /* INCLUDED_GSARRAY_BEAMFORMER_IMPL_H */

