/* -*- c++ -*- */

#define GSARRAY_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "gsarray_swig_doc.i"

%{
#include "gsarray/beamformer.h"
#include "gsarray/collector.h"
#include "gsarray/algorithm.h"
#include "gsarray/lcmv.h"
#include "gsarray/phaseshift.h"
%}

%include "gsarray/beamformer.h"
GR_SWIG_BLOCK_MAGIC2(gsarray, beamformer);
%include "gsarray/collector.h"
GR_SWIG_BLOCK_MAGIC2(gsarray, collector);
%include "gsarray/algorithm.h"
%include "gsarray/lcmv.h"
%include "gsarray/phaseshift.h"
