/* -*- c++ -*- */
/*
 * Copyright 2019 gr-gsarray author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_GSARRAY_BEAMFORMER_H
#define INCLUDED_GSARRAY_BEAMFORMER_H

#include <gsarray/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace gsarray {

    /*!
     * \brief <+description of block+>
     * \ingroup gsarray
     *
     */
    class GSARRAY_API beamformer : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<beamformer> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of gsarray::beamformer.
       *
       * To avoid accidental use of raw pointers, gsarray::beamformer's
       * constructor is in a private implementation
       * class. gsarray::beamformer::make is the public interface for
       * creating new instances.
       */
      static sptr make(size_t num_streams, uint8_t array_t, double spacing,
			   double lambda, uint8_t method);
    };

  } // namespace gsarray
} // namespace gr

#endif /* INCLUDED_GSARRAY_BEAMFORMER_H */

