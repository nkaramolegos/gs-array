options:
  parameters:
    author: ''
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: example_ps
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: example_ps
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: ampl
  id: variable
  parameters:
    comment: ''
    value: 200e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1152, 17]
    rotation: 0
    state: true
- name: c
  id: variable
  parameters:
    comment: ''
    value: '299792458'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [951, 18]
    rotation: 0
    state: true
- name: fc
  id: variable
  parameters:
    comment: ''
    value: 435e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1062, 18]
    rotation: 0
    state: true
- name: l
  id: variable
  parameters:
    comment: ''
    value: c/fc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [730, 17]
    rotation: 0
    state: true
- name: num_elements
  id: variable
  parameters:
    comment: ''
    value: '4'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [627, 18]
    rotation: 0
    state: true
- name: samp_rate_tx
  id: variable
  parameters:
    comment: ''
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 12]
    rotation: 0
    state: enabled
- name: satellite_rx_antenna
  id: variable_antenna
  parameters:
    aperture_efficiency: '55'
    beamwidth: '360'
    boom_length: '2.35'
    circumference: '1'
    comment: ''
    diameter: '5.4'
    frequency: 435.765e6
    gain: '6'
    lp: '0.5'
    pointing_error: '0'
    polarization: '3'
    rolloff_gain: '0'
    turn_spacing: '0.25'
    turns: '10'
    type: '5'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [365, 543]
    rotation: 0
    state: enabled
- name: satellite_tx_antenna
  id: variable_antenna
  parameters:
    aperture_efficiency: '55'
    beamwidth: '50'
    boom_length: '2.35'
    circumference: '1'
    comment: ''
    diameter: '5.4'
    frequency: 435.769e6
    gain: '14'
    lp: '0.5'
    pointing_error: '0'
    polarization: '3'
    rolloff_gain: '0'
    turn_spacing: '0.25'
    turns: '10'
    type: '5'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [173, 543]
    rotation: 0
    state: enabled
- name: spacing
  id: variable
  parameters:
    comment: ''
    value: l/2.0
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [845, 18]
    rotation: 0
    state: true
- name: tracker_rx_antenna
  id: variable_antenna
  parameters:
    aperture_efficiency: '0'
    beamwidth: '0'
    boom_length: '0'
    circumference: '0'
    comment: ''
    diameter: '0'
    frequency: 435.769e6
    gain: '0'
    lp: '0.5'
    pointing_error: '0'
    polarization: '2'
    rolloff_gain: '0'
    turn_spacing: '0'
    turns: '0'
    type: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [716, 543]
    rotation: 0
    state: enabled
- name: tracker_tx_antenna
  id: variable_antenna
  parameters:
    aperture_efficiency: '55'
    beamwidth: '50'
    boom_length: '2.35'
    circumference: '1'
    comment: ''
    diameter: '5.4'
    frequency: 435.765e6
    gain: '14'
    lp: '0.5'
    pointing_error: '0'
    polarization: '0'
    rolloff_gain: '0'
    turn_spacing: '0.25'
    turns: '10'
    type: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [556, 543]
    rotation: 0
    state: enabled
- name: variable_constellation_0
  id: variable_constellation
  parameters:
    comment: ''
    const_points: '[-1-1j, -1+1j, 1+1j, 1-1j]'
    dims: '1'
    precision: '8'
    rot_sym: '4'
    soft_dec_lut: None
    sym_map: '[0, 1, 3, 2]'
    type: bpsk
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [417, 16]
    rotation: 0
    state: true
- name: variable_leo_model_def_0
  id: variable_leo_model_def
  parameters:
    atmo_gases_attenuation: '2'
    comment: ''
    doppler_shift_enum: '0'
    fspl_attenuation_enum: '5'
    mode: '1'
    pointing_attenuation_enum: '0'
    precipitation_attenuation: '4'
    rainfall_rate: '90'
    surface_watervap_density: '7.5'
    temperature: '20'
    tracker: variable_tracker_0
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [7, 691]
    rotation: 0
    state: enabled
- name: variable_leo_model_def_1
  id: variable_leo_model_def
  parameters:
    atmo_gases_attenuation: '2'
    comment: ''
    doppler_shift_enum: '0'
    fspl_attenuation_enum: '5'
    mode: '1'
    pointing_attenuation_enum: '0'
    precipitation_attenuation: '4'
    rainfall_rate: '90'
    surface_watervap_density: '7.5'
    temperature: '20'
    tracker: variable_tracker_0
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1283, 547]
    rotation: 0
    state: enabled
- name: variable_leo_model_def_2
  id: variable_leo_model_def
  parameters:
    atmo_gases_attenuation: '2'
    comment: ''
    doppler_shift_enum: '0'
    fspl_attenuation_enum: '5'
    mode: '1'
    pointing_attenuation_enum: '0'
    precipitation_attenuation: '4'
    rainfall_rate: '90'
    surface_watervap_density: '7.5'
    temperature: '20'
    tracker: variable_tracker_0
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [291, 692]
    rotation: 0
    state: enabled
- name: variable_leo_model_def_3
  id: variable_leo_model_def
  parameters:
    atmo_gases_attenuation: '2'
    comment: ''
    doppler_shift_enum: '0'
    fspl_attenuation_enum: '5'
    mode: '1'
    pointing_attenuation_enum: '0'
    precipitation_attenuation: '4'
    rainfall_rate: '90'
    surface_watervap_density: '7.5'
    temperature: '20'
    tracker: variable_tracker_0
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [573, 693]
    rotation: 0
    state: enabled
- name: variable_leo_model_def_5
  id: variable_leo_model_def
  parameters:
    atmo_gases_attenuation: '2'
    comment: ''
    doppler_shift_enum: '0'
    fspl_attenuation_enum: '5'
    mode: '1'
    pointing_attenuation_enum: '0'
    precipitation_attenuation: '4'
    rainfall_rate: '90'
    surface_watervap_density: '7.5'
    temperature: '20'
    tracker: variable_tracker_0
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [858, 748]
    rotation: 0
    state: enabled
- name: variable_satellite_0
  id: variable_satellite
  parameters:
    comm_freq_rx: 435e6
    comm_freq_tx: 435e6
    comment: ''
    rx_antenna: satellite_rx_antenna
    tle_1: 1 25544U 98067A   18268.52547184  .00016717  00000-0  10270-3 0  9019
    tle_2: 2 25544  51.6373 238.6885 0003885 206.9748 153.1203 15.53729445 14114
    tle_title: UPSAT
    tx_antenna: satellite_tx_antenna
    value: '''ok'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [5, 543]
    rotation: 0
    state: enabled
- name: variable_tracker_0
  id: variable_tracker
  parameters:
    comm_freq_rx: 435e6
    comm_freq_tx: 435E6
    comment: ''
    gs_alt: '1'
    gs_lat: '35.3333'
    gs_lon: '25.1833'
    obs_end: '2018-09-25T15:58:35.0000000Z'
    obs_start: '2018-09-25T15:48:25.0000000Z'
    rx_antenna: tracker_rx_antenna
    satellite_info: variable_satellite_0
    time_resolution_us: '1000'
    tx_antenna: tracker_tx_antenna
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [891, 543]
    rotation: 0
    state: enabled
- name: analog_random_source_x_0
  id: analog_random_source_x
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    max: '2'
    maxoutbuf: '0'
    min: '0'
    minoutbuf: '0'
    num_samps: '1000'
    repeat: 'True'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [-8, 97]
    rotation: 0
    state: true
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: ampl
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [52, 286]
    rotation: 0
    state: enabled
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: 100e3
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1409, 173]
    rotation: 0
    state: true
- name: digital_chunks_to_symbols_xx_0
  id: digital_chunks_to_symbols_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dimension: '2'
    in_type: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    out_type: complex
    symbol_table: '[-1,1]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [219, 109]
    rotation: 0
    state: true
- name: gsarray_beamformer_0
  id: gsarray_beamformer
  parameters:
    affinity: ''
    alias: ''
    arr_conf: '1'
    comment: ''
    el_space: spacing
    la: l
    maxoutbuf: '0'
    method: '1'
    minoutbuf: '0'
    num_streams: num_elements
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1152, 160.0]
    rotation: 0
    state: true
- name: gsarray_collector_0
  id: gsarray_collector
  parameters:
    affinity: ''
    alias: ''
    arr_conf: '1'
    comment: ''
    el_space: spacing
    la: l
    maxoutbuf: '0'
    minoutbuf: '0'
    num_elements: num_elements
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [361, 291]
    rotation: 0
    state: true
- name: leo_channel_model_0
  id: leo_channel_model
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    imp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    model: variable_leo_model_def_0
    noise_type: '1'
    sample_rate: samp_rate_tx
    snr: '-100'
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [802, 99]
    rotation: 0
    state: enabled
- name: leo_channel_model_0_0
  id: leo_channel_model
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    imp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    model: variable_leo_model_def_1
    noise_type: '1'
    sample_rate: samp_rate_tx
    snr: '-100'
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [807, 208]
    rotation: 0
    state: enabled
- name: leo_channel_model_0_0_0
  id: leo_channel_model
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    imp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    model: variable_leo_model_def_2
    noise_type: '1'
    sample_rate: samp_rate_tx
    snr: '-100'
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 317]
    rotation: 0
    state: enabled
- name: leo_channel_model_0_0_0_0
  id: leo_channel_model
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    imp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    model: variable_leo_model_def_3
    noise_type: '1'
    sample_rate: samp_rate_tx
    snr: '-100'
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [805, 422]
    rotation: 0
    state: enabled
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate_tx
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: '0'
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1421, 424]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'True'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'True'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '1024'
    srate: samp_rate_tx
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1514, 302]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'True'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'True'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '"Before"'
    nconnections: '1'
    size: '1024'
    srate: samp_rate_tx
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1184, 454]
    rotation: 0
    state: true

connections:
- [analog_random_source_x_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [blocks_multiply_const_vxx_0, '0', gsarray_collector_0, '0']
- [blocks_multiply_const_vxx_0, '0', gsarray_collector_0, '1']
- [blocks_multiply_const_vxx_0, '0', gsarray_collector_0, '2']
- [blocks_multiply_const_vxx_0, '0', gsarray_collector_0, '3']
- [blocks_throttle_0, '0', qtgui_freq_sink_x_0, '0']
- [blocks_throttle_0, '0', qtgui_time_sink_x_0, '0']
- [digital_chunks_to_symbols_xx_0, '0', blocks_multiply_const_vxx_0, '0']
- [gsarray_beamformer_0, '0', blocks_throttle_0, '0']
- [gsarray_collector_0, '0', leo_channel_model_0, '0']
- [gsarray_collector_0, '1', leo_channel_model_0_0, '0']
- [gsarray_collector_0, '2', leo_channel_model_0_0_0, '0']
- [gsarray_collector_0, '3', leo_channel_model_0_0_0_0, '0']
- [leo_channel_model_0, '0', gsarray_beamformer_0, '0']
- [leo_channel_model_0_0, '0', gsarray_beamformer_0, '1']
- [leo_channel_model_0_0_0, '0', gsarray_beamformer_0, '2']
- [leo_channel_model_0_0_0_0, '0', gsarray_beamformer_0, '3']
- [leo_channel_model_0_0_0_0, '0', qtgui_time_sink_x_0_0, '0']

metadata:
  file_format: 1
